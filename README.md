# Docker Assistant

This application is planned to function as a client/server to the Docker daemon.

It will provide:
1. a standard API across Docker API versions
1. An accessible interface to `unix://var/run/docker.sock`
1. a Rust API for standard Docker commands (e.g. `Containers::list`)
## Wait a second doesn't Docker already provide a RESTful interface?

Yes it does. But this app provides the following benefits:
1. Decouples clients with the Docker daemon
1. Provide a standard API across Docker versions
1. Minimizes configuration needed to interact with Docker daemon

## Demo of Current Functionality

    fn main() -> std::io::Result<()> {
    
      // list existing containers
      for container in Containers::list() {
        println!("{:?}", container);
      }
    
      // try and create a container
      match Containers::create(CreateContainerMetadata {
        Image: String::from("postgres")
      }) {
        Some(new_container) => {
          let container_id = new_container.Id;
          println!("Container id is {}", container_id);
          // start it if it exists
          Containers::start(container_id.as_str());
          // call the api to stop it here
          
        },
        None => println!("container doesn't exist")
      }
    
    
      Ok(())
    }
