extern crate serde_json;
extern crate serde;

use crate::docker::Containers;
use crate::docker::rest::api::Container;
use crate::docker::socket::api::{CreateContainerMetadata, CreatedContainerSummary, ContainerSummary};
use rocket_contrib::json::Json;
use rocket::http::Status;

#[get("/containers")]
fn list() -> String {
  serde_json::to_string(&Containers::list()).unwrap()
}

#[get("/containers/<id>")]
fn get(id: String) -> Json<ContainerSummary> {
  for container in Containers::list() {
    if container.id == id {
      return Json(container);
    }
  }
  return Json(ContainerSummary {
    id: String::new(), names: Vec::new(),image: String::new(), state: String::new()
  })
}

#[post("/containers/<id>/stop")]
fn stop(id: String) -> Status {
  for container in Containers::list() {
    if container.id == id {
      Containers::stop(id.as_str());
      return  Status::Accepted;
    }
  }
  return Status::NotFound;
}

#[delete("/containers/<id>?<stop>")]
fn delete(id: String, stop: bool) -> Status {
  for container in Containers::list() {
    if container.id == id {
      if container.state.to_lowercase().trim() == "running" {
        if stop {
          Containers::stop(id.as_str());
        } else {
          return Status::BadRequest;
        }
      }
      Containers::delete(id.as_str());
      return  Status::Accepted;
    }
  }
  return Status::NotFound;
}

#[post("/containers", data = "<input>")]
fn create(input: String) -> Json<CreatedContainerSummary> {
  let container : Container = serde_json::from_str(input.as_str()).unwrap();
  let metadata: CreateContainerMetadata = container.as_socket_container();
  match Containers::create(metadata) {
    Some(summary) => {
      Containers::start(summary.Id.as_str());
      Json(summary)
    },
    None => Json(CreatedContainerSummary{Id: String::new(), Warnings: Vec::new()})
  }
}

pub fn rocket() -> rocket::Rocket {
  return rocket::ignite().mount("/", routes![list, create, get, stop, delete]);
}

pub fn start() {
  rocket().launch();
}

#[cfg(test)]
mod tests {
  use crate::http::client::local::Client as TestClient;
  use crate::rocket::http::Status;
  use crate::docker::rest::api::Container;
  use std::collections::HashMap;

  #[test]
  fn create_container() {
    let client : TestClient = TestClient::new(super::rocket());
    let container = Container{
      name: String::from("bar"),
      // need to add functionality for containers that don't yet exist locally
      image: String::from("postgres"),
      environment: HashMap::new(),
      ports: vec![]
    };
    // return response with contains status, headers, and body
    let response = client.create(container);
    assert_eq!(response.status, Status::Ok);
    match response.body {
      Some(body) => {
        assert_eq!(response.status, Status::Ok);
        assert!(!body.Id.is_empty())

      },
      None => assert!(false)
    }
  }

  #[test]
  fn list_containers() {
    let client : TestClient = TestClient::new(super::rocket());
    // return response with contains status, headers, and body
    let response = client.list();
    assert_eq!(response.status, Status::Ok);
  }
}
