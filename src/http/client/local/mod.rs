use crate::rocket::local::Client as RocketClient;
use crate::docker::socket::api::{ContainerSummary, CreatedContainerSummary};
use crate::docker::rest::api::Container;
use crate::rocket::Rocket;
use crate::rocket::http::Status;

pub struct Client {
   rocket_client: RocketClient
}

pub struct Response<T> {
  pub body: T,
  pub status: Status
}

impl Client {
  pub fn list(&self) -> Response<Vec<ContainerSummary>> {
    let mut response = self.rocket_client.get("/containers").dispatch();
    match response.body_string() {
      Some(body) => {
        return Response{
         body: serde_json::from_str(body.as_str()).unwrap(), status: response.status()
        }
      }
      None => Response{body: vec![], status:  response.status()}
    }
  }

  pub fn create(&self, container: Container) -> Response<Option<CreatedContainerSummary>> {
    let payload = serde_json::to_vec(&container).unwrap();
    let mut response = self.rocket_client.post("/containers").body(payload).dispatch();
    let deserialized_body: Option<CreatedContainerSummary> = response.body_string().map(|body| {
      serde_json::from_str(body.as_str()).unwrap()
    });

    return Response{body: deserialized_body, status: response.status()}
  }

  pub fn new(rocket: Rocket) -> Client {
    let client : RocketClient = RocketClient::new(rocket).expect("valid rocket instance");
    return Client{rocket_client: client}
  }
}
