extern crate serde;
extern crate serde_json;

use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

use crate::docker::socket::api::CreateContainerMetadata;

#[derive(Serialize, Deserialize, Debug)]
pub struct Container {
  #[serde(default)]
  pub name: String,
  pub image: String,
  #[serde(default)]
  pub environment: HashMap<String, String>,
  #[serde(default)]
  pub ports: Vec<PortMapping>
}

//  #[serde(default)]
//  pub status: ContainerStatus


#[derive(Serialize, Deserialize, Debug)]
pub struct PortMapping {
  pub public: u8,
  pub private: u8,
  #[serde(default)]
  pub prototcol: PortProtocol

}

#[derive(Serialize, Deserialize, Debug)]
pub enum PortProtocol {
  #[serde(rename = "tcp")]
  Tcp,
  #[serde(rename = "udp")]
  Udp
}

impl Default for PortProtocol {
  fn default() -> Self {
    PortProtocol::Tcp
  }
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ContainerStatus {
  #[serde(rename = "new")]
  New,
  #[serde(rename = "created")]
  Created,
  #[serde(rename = "running")]
  Running,
  #[serde(rename = "stopped")]
  Stopped
}

impl Default for ContainerStatus {
  fn default() -> Self {
    ContainerStatus::New
  }
}

impl Container {
  pub fn as_socket_container(&self) -> CreateContainerMetadata {

    let image = &self.image;
    let environment = &self.environment;

    let mut env: Vec<String> = Vec::new();
    for (key, value) in environment {
      env.push(format!("{}={}", key, value))
    }
    return CreateContainerMetadata {
      Image: image.to_string(),
      Env: env
    }
  }
}
