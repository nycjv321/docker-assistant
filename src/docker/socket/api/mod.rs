extern crate serde_json;
extern crate serde;

use serde_json::{Value};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct CreateContainerMetadata {
  pub Image: String,
  pub Env: Vec<String>
}

enum ContainerStatus {
  // An `enum` may either be `unit-like`,
  Created,
  Restarting,
  Running,
  Removing,
  Paused,
  Existed,
  Dead,
}


#[derive(Serialize, Deserialize, Debug)]
pub struct ContainerSummary {
  pub id: String,
  pub names: Vec<String>,
  pub image: String,
  pub state: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CreatedContainerSummary {
  pub Id: String,
  pub Warnings: Vec<String>
}
