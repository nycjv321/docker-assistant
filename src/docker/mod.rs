extern crate serde_json;
extern crate serde;

pub mod socket;
pub mod rest;

use std::io::prelude::*;
use std::net::Shutdown;
use std::os::unix::net::UnixStream;
use std::time::Duration;

use socket::api::{CreateContainerMetadata, ContainerSummary, CreatedContainerSummary};

use serde_json::{Value};
use serde::{Serialize, Deserialize};

fn create_stream() -> UnixStream {
  let socket = match UnixStream::connect("/var/run/docker.sock") {
    Ok(sock) => sock,
    Err(e) => {
      println!("Got error: {:?}", e);
      std::process::exit(1);
    }
  };

  if let Ok(Some(err)) = socket.take_error() {
    println!("Got different error: {:?}", err);
    std::process::exit(1);
  } else {
    socket.set_read_timeout(Some(Duration::new(5, 0))).expect("Couldn't set read timeout");
    return socket;
  }
}


pub struct Containers {}

impl Containers {

  pub fn create(metadata: CreateContainerMetadata) -> Option<CreatedContainerSummary> {
    let mut stream = create_stream();

    let payload = serde_json::to_string(&metadata).unwrap_or(String::from(""));

    let request = format!("{}{}", format!("POST /v1.12/containers/create HTTP/1.1\r\nContent-Length: {}\r\nContent-Type: application/json\r\nHost: localhost\r\nUser-Agent: curl/7.64.1\r\nAccept: */*\r\n\r\n", payload.as_bytes().len()) , payload);
    stream.write_all(request.as_bytes()).unwrap_or(());
    stream.shutdown(Shutdown::Write).unwrap_or(());

    let mut response = String::new();
    stream.read_to_string(&mut response).unwrap_or(0);

    let mut split_input = response.split("\r\n\r\n");
    let headers = split_input.next().unwrap_or("no headers");
    let body = split_input.next().unwrap_or("no body");
    let mut split_headers = headers.split("\r\n");

    let status = split_headers.next().unwrap_or("no status line");
    if status != "HTTP/1.1 201 Created" {
      println!("{:?}", response);
      //TODO log error
      return None;
    } else {
      let response : CreatedContainerSummary = serde_json::from_str(body).unwrap();
      return Some(response);

    }
  }

  pub fn stop(id: &str) -> () {
    let mut stream = create_stream();
    let request = format!("POST /v1.12/containers/{id}/stop HTTP/1.1\r\nHost: localhost\r\nUser-Agent: curl/7.64.1\r\nAccept: */*\r\n\r\n", id = id);
    stream.write_all(request.as_bytes()).unwrap_or(());
    stream.shutdown(Shutdown::Write).unwrap_or(());

    let mut response = String::new();
    stream.read_to_string(&mut response).unwrap_or(0);

    let mut split_input = response.split("\r\n\r\n");
    let headers = split_input.next().unwrap_or("no headers");
    let mut split_headers = headers.split("\r\n");

    let status = split_headers.next().unwrap_or("no status line");
    if status != "HTTP/1.1 304 Not Modified" && status != "HTTP/1.1 204 No Content" {
      println!("unable to stop container");
    }
  }

  pub fn delete(id: &str) -> () {
    let mut stream = create_stream();
    let request = format!("DELETE /v1.12/containers/{id}/v1 HTTP/1.1\r\nHost: localhost\r\nUser-Agent: curl/7.64.1\r\nAccept: */*\r\n\r\n", id = id);
    stream.write_all(request.as_bytes()).unwrap_or(());
    stream.shutdown(Shutdown::Write).unwrap_or(());

    let mut response = String::new();
    stream.read_to_string(&mut response).unwrap_or(0);

    let mut split_input = response.split("\r\n\r\n");
    let headers = split_input.next().unwrap_or("no headers");
    let mut split_headers = headers.split("\r\n");

    let status = split_headers.next().unwrap_or("no status line");
    if status != "HTTP/1.1 304 Not Modified" && status != "HTTP/1.1 204 No Content" {
      println!("unable to delete container");
    }
  }

  pub fn start(id: &str) -> () {
    let mut stream = create_stream();
    let request = format!("POST /v1.12/containers/{id}/start HTTP/1.1\r\nHost: localhost\r\nUser-Agent: curl/7.64.1\r\nAccept: */*\r\n\r\n", id = id);
    stream.write_all(request.as_bytes()).unwrap_or(());
    stream.shutdown(Shutdown::Write).unwrap_or(());

    let mut response = String::new();
    stream.read_to_string(&mut response).unwrap_or(0);

    let mut split_input = response.split("\r\n\r\n");
    let headers = split_input.next().unwrap_or("no headers");
    let mut split_headers = headers.split("\r\n");

    let status = split_headers.next().unwrap_or("no status line");
    if status != "HTTP/1.1 304 Not Modified" && status != "HTTP/1.1 204 No Content" {
      println!("unable to start container");
    }
  }

  pub fn list() -> Vec<ContainerSummary> {
    let mut stream = create_stream();
    let request = b"GET /v1.12/containers/json HTTP/1.1\r\nHost: localhost\r\nUser-Agent: curl/7.64.1\r\nAccept: */*\r\n\r\n";
    stream.write_all(request).unwrap_or(());
    stream.shutdown(Shutdown::Write).unwrap_or(());

    let mut response = String::new();
    stream.read_to_string(&mut response).unwrap_or(0);

    let mut split_input = response.split("\r\n\r\n");
    let headers = split_input.next().unwrap_or("no headers");
    let body = split_input.next().unwrap_or("{}");

    println!("{}\n\n\n", body);

    let v: Value = serde_json::from_str(body).unwrap_or(Value::Array(Vec::new()));

    let values = match v.as_array() {
      Some(a) => {
        a
      }
      None => {
        println!("entity is not array");
        std::process::exit(1);
      }
    };

    let mut containers: Vec<ContainerSummary> = Vec::new();


    for value in values {
      match value.as_object() {
        Some(a) => {
          let id = as_string(a.get("Id"));
          let image = as_string(a.get("Image"));
          let state = as_string(a.get("State"));
          let names = as_string_vec(a.get("Names"));

          let container = ContainerSummary {id, names, image, state};
          containers.push(container);
        }
        None => {
          println!("no entities found");
          std::process::exit(1);
        }
      };
    }
    return containers;
  }
}

fn as_string(a: Option<&Value>) -> String {
  let x = match a {
    Some(id) => id.as_str().unwrap_or(""),
    None => ""
  };
  return String::from(x);
}


fn as_string_vec(a: Option<&Value>) -> Vec<String> {
  let x = match a {
    Some(values) => {
      let values_array = values.as_array();
      return match values_array {
        Some(unwrapped) => {
          let x1 = unwrapped.into_iter().map(|x| String::from(x.as_str().unwrap_or(""))).collect::<Vec<_>>();
          x1
        }
        None => Vec::new()
      };
    }
    None => Vec::new()
  };
  return x;
}
